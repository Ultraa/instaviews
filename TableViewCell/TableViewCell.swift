//
//  TableViewCell.swift
//  InstaViews
//
//  Created by User Userovich on 28.11.17.
//  Copyright © 2017 User Userovich. All rights reserved.
//

import UIKit

struct CellStruct: Decodable {
    var image: String?
    var likes: UInt32?
    var comments: UInt32?
    var geo: Location?
    var caption: CaptionStruct?
}

class TableViewCell: UITableViewCell {

    // MARK: Переменные
    
    /// Сюда приходят данные для ячейки
    var cellModel: CellStruct? { didSet {
        self.layoutCell()
        } }
    /// Фото в ячейке
    @IBOutlet weak var logoImage: UIImageView!
    ///  Количество лайков
    @IBOutlet weak var likeLabel: UILabel!
    /// Кол-во комментариев
    @IBOutlet weak var countLabel: UILabel!
    /// Наличие геолокации (кнопка)
    @IBOutlet weak var buttonGPS: UIButton!
    /// Подпись к картинке
    @IBOutlet weak var captionText: UITextView!
    @IBOutlet weak var blurImage: UIImageView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareForReuse()
        self.layoutCell()
    }
    
    // MARK: Main logic (Основная логика класса)
    
    /// Заполнение ячейки данными
    func layoutCell() {
        guard let urlToLoad = cellModel?.image, let likes = cellModel?.likes, let comments = cellModel?.comments else {
            return
        }
        
        let url = URL(string: urlToLoad)
        self.logoImage.kf.setImage(with: url)
        self.likeLabel.text = "\(likes)"
        self.countLabel.text = "\(comments)"
        if (cellModel?.geo?.name) != nil {
            buttonGPS.isHidden = false
        }
        if let text = (cellModel?.caption?.text) {
            self.captionText.text = text
            self.captionText.isScrollEnabled = false
        }
        self.blurImage.kf.setImage(with: url)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.buttonGPS.isHidden = true
        self.captionText.text = ""
        self.likeLabel.text = "0"
        self.countLabel.text = "0"
    }
}
