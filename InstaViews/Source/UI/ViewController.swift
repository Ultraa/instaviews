//
//  ViewController.swift
//  InstaViews
//
//  Created by User Userovich on 28.11.17.
//  Copyright © 2017 User Userovich. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import Alamofire
import Lock

/// Класс в котором будут загружены фотографии из Instagram по accessToken
class ViewController: UIViewController, UITableViewDataSource {
    
    // MARK: Переменные
    
    /// Таблица с фотографиями на основном экране
    @IBOutlet weak var tableView: UITableView!
    
    /// Переменная, куда получаем accessToken с другого ViewController'a
    var accessToken: String?
    /// Распарсенный JSON-объект
    var info: Base?
    /// Состояние переключателя перехода на Увеличенное_фото / Карту
    var switched: Bool = true

    // MARK: Life cycle (Жизненный цикл класса)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "InstaViews"
        tableView.dataSource = self
        tableView.delegate = self
        
        /// Подключаем кастомную ячейку из TableViewCell.xib
        let nibName = UINib(nibName: "TableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "tableViewCell")
        
        if let accessToken = accessToken {
            let urlString = "https://api.instagram.com/v1/users/self/media/recent/?access_token=\(accessToken)"
            
            guard let url = URL(string: urlString) else { return }
            let queue = DispatchQueue.global(qos: .background)
            
            queue.async {
                URLSession.shared.dataTask(with: url) { [weak self] (data, _, error) in
                    guard let data = data else { return }
                    guard error == nil else { return }
                    do {
                        let information = try JSONDecoder().decode(Base.self, from: data) // в .decode()  пишем свой тип данных (который описывает структуру JSON-файла), а в from: пишем data - то есть куда мы получили данные из URL-запроса
                        self?.info = information
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    } catch let error {
                        print(error)
                    }
                    }.resume() // закрываем и продолжаем urlsession
            }
        }
    }
    
    // Сообщение об деинициализации контроллера (required)
    deinit {
        #if DEBUG
            print("✅ \(type(of: self)): deinit")
        #endif
    }
    
    // MARK: Main logic (Основная логика класса)
    
    /// Переключатель перехода на Увеличенное_фото / Карту
    @IBAction func switchBetweenPhotoMap(_ sender: UISwitch) {
        if sender.isOn == true {
            switched = true                                     // Если переключатель включен, то переходим на экран с увеличенным фото
        } else {
            switched = false                                    // Иначе переходим на экран с геолокацией
        }
    }
    
    /// показывает окно об ошибке, если фото не содержит гео-данных
    fileprivate func showAlert() {
        let alert = UIAlertController(title: "Warning!", message: "For this photo no location!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// Для работы с таблицей
extension ViewController: UITableViewDelegate {
    
    // количество ячеек в таблице
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Оберточка для безопасненького возвращеньица
        if let actualInfo = self.info {
            return actualInfo.data.count
        } else {
            return 0
        }
    }
    
    // как задавать ячейку и какую информацию в неё класть
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as? TableViewCell else {
            return UITableViewCell()
        }
        
        // Передаем параметры ячейки в структуре, которая содержит эти параметры в качестве своих полей
        guard let actualUrl = info?.data[indexPath.item].images.standard_resolution.url,            // Если у нас есть адрес изображения
            let actualLikes = info?.data[indexPath.item].likes.count,                             // и Если есть информация о лайках
            let actualComments = info?.data[indexPath.item].comments.count else { return cell }    // и Если есть информация о комментариях, то идем дальше
        let cellparam = CellStruct(image: actualUrl, likes: actualLikes, comments: actualComments, geo: info!.data[indexPath.item].location, caption: info!.data[indexPath.item].caption)
        cell.cellModel = cellparam // cellModel - переменная из TableViewCell контроллера
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.deselectRow()  // Отменяем выделение ячейки после её выбора
        
        if switched == true { // Показываем фото на отдельном PhotoViewController
            guard let photoView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController else {
                return
            }
            
            if let actualUrl = info?.data[indexPath.item].images.standard_resolution.url {
                photoView.urlString = actualUrl
            }
            
            photoView.modalPresentationStyle = .overCurrentContext      // Стиль презентации экрана с фото
            photoView.modalTransitionStyle = .crossDissolve            // Стиль перехода на экран с фото
            self.present(photoView, animated: true)                  // Показываем окно с фото поверх экрана с таблицей всех фото
        } else {
            // Показываем другой ViewController, для карты
            guard let map = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController else {
                return
            }
            if let actualGeo = info?.data[indexPath.item].location {
                let mapGeo = CellStruct(image: nil, likes: nil, comments: nil, geo: actualGeo, caption: nil)
                
                map.mapModel = mapGeo                               // Передаем информацию о геолокации в переменную с экраном карты
                map.modalTransitionStyle = .partialCurl               // Стиль перехода на экран с картой
                self.navigationController?.pushViewController(map, animated: true) // Переходим на экран с картой
            } else {
                self.showAlert()                                     // Если нет данных геолокации, показываем Алерт
                return
            }
        }
    }
    
    /// Отменяем выделение выбранной ячейки
    func deselectRow() {
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
}
