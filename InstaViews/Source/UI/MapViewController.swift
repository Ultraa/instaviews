//
//  MapViewController.swift
//  InstaViews
//
//  Created by User Userovich on 15.12.17.
//  Copyright © 2017 User Userovich. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {

    // MARK: Переменные
    
    /// Сюда получаем данные для отображения геолокации
    var mapModel: CellStruct?
    
    @IBOutlet weak var viewMap: GMSMapView!
    
    // MARK: Life cycle (Жизненный цикл класса)
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "MapView"
        // Do any additional setup after loading the view.
    }
    
    // Вот-вот отобразится
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupController()
    }
    
    // Уже отобразился
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Приближение камеры после загрузки экрана
        let updateCamera = GMSCameraUpdate.zoom(to: 15)
        viewMap.animate(with: updateCamera)
    }
    
    // Сообщение об деинициализации контроллера (required)
    deinit {
        #if DEBUG
            print("✅ \(type(of: self)): deinit")
        #endif
    }
    
    // MARK: Main logic (Основная логика класса)
    
    func setupController() {
        guard let lat = mapModel?.geo?.latitude, let long = mapModel?.geo?.longitude else {
            return
        }
        
        let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let camera = GMSCameraPosition.camera(withTarget: Coordinates, zoom: 10)
        viewMap.camera = camera
        viewMap.settings.compassButton = true
        let marker = GMSMarker()
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        
        // Меняем интерфейс, если уже темно
        if hour >= 16 || hour < 8 {
            do {
                if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                    viewMap.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                } else {
                    print("Unable to find style.json")
                }
            } catch {
                print("One or more of the map styles failed to load. \(error)")
            }
        }
        
        marker.position = camera.target
        marker.snippet = mapModel?.geo?.name
        marker.appearAnimation = .pop
        marker.map = viewMap
    }
    
    func mapCoordinates() {
        guard let lat = mapModel?.geo?.latitude, let long = mapModel?.geo?.longitude else {
            return
        }
        
        let Coordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
        print("Coordinates recieved:", lat, "&", long)
        let camera = GMSCameraPosition.camera(withTarget: Coordinates, zoom: 5)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        let marker = GMSMarker()
        
        marker.position = camera.target
        marker.snippet = mapModel?.geo?.name
        marker.appearAnimation = .pop
        marker.map = mapView
    }
    
}
