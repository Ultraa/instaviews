//
//  PhotoViewController.swift
//  InstaViews
//
//  Created by User Userovich on 28.11.17.
//  Copyright © 2017 User Userovich. All rights reserved.
//

import UIKit

/// Класс в котором отображается увеличенная картинка с ViewController'a, которую можно увеличивать и убирать свайпом -> тогда возвращаемся на экран таблицы ViewController
class PhotoViewController: UIViewController {
    
    // MARK: Переменные
    
    /// Интернет-адрес
    var urlString: String?
    /// Вьюха скролла на экране
    @IBOutlet weak var scrollBigView: UIScrollView!
    /// Вьюха картинки на скролле
    @IBOutlet weak var imageBigView: UIImageView!
    /// Blur-эффект для заднего фона
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let urlString = urlString {
            let url = URL(string: urlString)
            self.imageBigView.kf.setImage(with: url)
        }
        scrollBigView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Делаем Blur на весь экран независимо от положения экрана
        self.blurEffect.frame = self.view.bounds
        self.blurEffect.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    // Сообщение об деинициализации контроллера (required)
    deinit {
        #if DEBUG
            print("✅ \(type(of: self)): deinit")
        #endif
    }

    // MARK: Main logic (Основная логика класса)
    
    /// Отвечает за свайп картинки
    @IBAction func panPhotoView(_ sender: UIPanGestureRecognizer) {
        let PhotoView = sender.view!
        let point = sender.translation(in: view)
        
        PhotoView.center = CGPoint(x: PhotoView.center.x + point.x, y: PhotoView.center.y + point.y)                                  // Запоминаем центр картинки
        
        if sender.state == UIGestureRecognizerState.ended {
            let velocity = sender.velocity(in: self.view)
            if velocity.y >= 500 {                                                                                                  // Если сильно переместили картинку вниз
                UIView.animate(withDuration: 1, delay: 0, animations: {
                    self.imageBigView.frame.origin = CGPoint(x: self.imageBigView.frame.origin.x, y: self.imageBigView.frame.size.height)
                    self.blurEffect.alpha = 0
                }, completion: { _ in                                                                                               // То, уходим с экрана
                    self.dismiss(animated: false, completion: nil)
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    PhotoView.center = self.view.center                                                                              // Иначе, возвращаем картинку на центр экрана
                })
            }
        }
        
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
}

// Расширение класса для масштабирования изображения
extension PhotoViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageBigView
    }
}
