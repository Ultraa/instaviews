//
//  LoginViewController.swift
//  InstaViews
//
//  Created by User Userovich on 08.12.17.
//  Copyright © 2017 User Userovich. All rights reserved.
//

import UIKit
import Auth0
import Reachability

// 6623132209.d8ff9c9.dbd7fd1dd007419380440e1a6c571020 // access.token instagram
// https://authfortest.eu.auth0.com/login/callback - эта строка была взята из instagram developer manage clients - valid redirect URIs
// flynmage://authfortest.eu.auth0.com/ios/flynmage/callback - эта строка была взята из manage auth0 clients - settings - allowed callback urls

class LoginViewController: UIViewController {
    
    // MARK: Переменные
    
    /// Кнопка авторизации Sign In
    @IBOutlet weak var signInButton: UIButton!
    /// Кнопка входа без авторизации Avoid the auth
    @IBOutlet weak var avoidButton: UIButton!
    
    /// Переменная проверки доступности интернет-соединения
    var reachability: Reachability?
    
    // MARK: Life cycle (Жизненный цикл класса)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Проверка интернет-соединения (используется библиотека Reachability
        self.reachability = Reachability.init()
        
        let hasConnection = (self.reachability!.connection) != .none
        [signInButton, avoidButton].forEach({ $0.alpha = hasConnection ? 1 : 0 })
        [signInButton, avoidButton].forEach({ $0.isUserInteractionEnabled = hasConnection })
        
        switch self.reachability!.connection {
        case .wifi:
            print("Reachable via Wi-Fi")
        case .cellular:
            print("Reachable via cellular")
        case .none:
            print("Network not reachable")
        }
    }
    
    // Вот-вот отобразится
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        
        if hour >= 16 || hour < 8 {
            navigationController?.navigationBar.barTintColor = UIColor.lightGray
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.blue ]
            self.view.backgroundColor = UIColor.lightGray
        }
    }
    
    // Сообщение об деинициализации контроллера (required)
    deinit {
        #if DEBUG
            print("✅ \(type(of: self)): deinit")
        #endif
    }
    
    // MARK: Main logic (Основная логика класса)
    
    /// Что происходит при нажатии на кнопку Sign In
    @IBAction func showLoginController(_ sender: UIButton) {
        guard let clientInfo = plistValues(bundle: Bundle.main) else { return }
        Auth0
            .webAuth()
            .scope("openid profile")
            .audience("https://" + clientInfo.domain + "/userinfo")
            .start { [weak self] in
                switch $0 {
                case .failure(let error):
                    print("Error: \(error)")
                case .success(_):
                    guard let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
                        return
                    }
                    vc.accessToken = "6623132209.d8ff9c9.dbd7fd1dd007419380440e1a6c571020"
                    vc.modalTransitionStyle = .flipHorizontal
                    self?.navigationController?.pushViewController(vc, animated: true) // Переходим на ViewController
                }
        }
    }
    
    /// Функция преобразования значений из файла Auth0.plist
    func plistValues(bundle: Bundle) -> (clientId: String, domain: String)? {
        guard
            let path = bundle.path(forResource: "Auth0", ofType: "plist"),
            let values = NSDictionary(contentsOfFile: path) as? [String: Any]
            else {
                print("Missing Auth0.plist file with 'ClientId' and 'Domain' entries in main bundle!")
                return nil
        }
        
        guard
            let clientId = values["ClientId"] as? String,
            let domain = values["Domain"] as? String
            else {
                print("Auth0.plist file at \(path) is missing 'ClientId' and/or 'Domain' entries!")
                print("File currently has the following entries: \(values)")
                return nil
        }
        return (clientId: clientId, domain: domain)
    }
    
    /// Что происходит при нажатии на кнопку Avoid the auth
    @IBAction func avoidAuth(_ sender: UIButton) {
        guard let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            return
        }
        vc.accessToken = "6623132209.d8ff9c9.dbd7fd1dd007419380440e1a6c571020"
        self.navigationController?.pushViewController(vc, animated: true) // Переходим на ViewController
    }
}
