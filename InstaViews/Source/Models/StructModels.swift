//
//  StructModels.swift
//  InstaViews
//
//  Created by User Userovich on 11.12.17.
//  Copyright © 2017 User Userovich. All rights reserved.
//

import Foundation

struct Base: Decodable {
    /// Массив записей
    var data: [Data]
}

struct Data: Decodable {
    /// Данные пользователя
    var user: User
    /// Фотографии пользователя
    var images: Images
    /// Подпись под изображением
    var caption: CaptionStruct?
    /// Количество лайков к фотографии
    var likes: Counts
    /// Количество комментариев к фотографии
    var comments: Counts
    /// Место, где была сделана фотография
    var location: Location?
}

struct User: Decodable {
    /// Идентификатор пользователя
    var id: String?
    /// Полное имя пользователя
    var full_name: String?
    /// Аватар пользователя
    var profile_picture: String?
    /// Никнейм
    var username: String?
}

struct Images: Decodable {
    /// Ярлык фотографии
    var thumbnail: WHU
    /// Фотография низкого разрешения
    var low_resolution: WHU
    /// Фотография нормального разрешения
    var standard_resolution: WHU
}

struct CaptionStruct: Decodable {
    /// Идентификатор подписи
    var id: String?
    /// Содержание подписи
    var text: String?
    /// Время создания подписи
    var createdTime: String?
}

struct Counts: Decodable {
    /// Количество чего-либо
    var count: UInt32
}

struct Location: Decodable {
    /// Широта
    var latitude: Double
    /// Долгота
    var longitude: Double
    /// Название места
    var name: String?
    /// Идентификатор места
    var id: UInt64
}

struct WHU: Decodable {
    /// Ширина фотографии
    var width: UInt32
    /// Высота фотографии
    var height: UInt32
    /// Интернет-адрес фотографии
    var url: String?
}
