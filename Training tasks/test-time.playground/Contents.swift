//: Playground - noun: a place where people can play

import UIKit
import PlaygroundSupport
//PlaygroundPage.current.needsIndefiniteExecution = true

//var str = "Hello, playground"
//
//let date = Date()
//let calendar = Calendar.current
//
//let hour = calendar.component(.hour, from: date)
//
//let operation1 = {
//    print("Start 1")
//    print("Finish 1")
//}
//
//
//func task(_ symbol: String) { for i in 1...10 {
//    print("\(symbol) \(i) приоритет = \(qos_class_self().rawValue)")
//    }
//}
//
//func taskHIGH(_ symbol: String) {
//    print("\(symbol) HIGH приоритет = \(qos_class_self().rawValue)")
//}
//
//let mainQueue = DispatchQueue.main
//let userInteractiveQueue = DispatchQueue.global(qos: .userInteractive)
//let userQueue = DispatchQueue.global(qos: .userInitiated)
//let utilityQueue = DispatchQueue.global(qos: .utility)
//let backgroundQueue = DispatchQueue.global(qos: .background)
//let defaultQueue = DispatchQueue.global()

//    let workerQueue1 = DispatchQueue(label: "com.bestkora.worker_concurrent1", qos: .userInitiated, attributes: .concurrent)
//    let workerQueue2 = DispatchQueue(label: "com.bestkora.worker_concurrent1", qos: .utility, attributes: .concurrent)
//    workerQueue1.async { task("tararararararara") }
//    workerQueue2.async { task("----------------") }
//    sleep(1)
//
//    let mySerialQueue = DispatchQueue(label: "com.bestkora.mySerial")
//    mySerialQueue.sync { task("tararararararara") }
//    mySerialQueue.async { task("----------------") }
//
//    let workerDelayQueue = DispatchQueue(label: "com.azaza.igorek", qos: .userInitiated, attributes: [.concurrent, .initiallyInactive])
//    workerDelayQueue.async { task("QWA QWA QWA") }
//
//    workerDelayQueue.activate()

//    let workerQueue = DispatchQueue(label: "com.azaza.serega", qos: .userInitiated, attributes: .concurrent)
//    workerQueue.async { task("tararararararara") }
//    workerQueue.async { task("----------------") }

// Такие задания с флагом .enforceQoS будут выполняться раньше остальных, если есть флаг, QoS - не учитывается!
//let highPriorityItem = DispatchWorkItem (qos: .background, flags:[.enforceQoS]){
//        taskHIGH("+")
//    }
//let highPriorityItem2 = DispatchWorkItem (qos: .default, flags:[.enforceQoS]){
//    taskHIGH("++")
//}
//    safeString.setString(string: "")
//    usualString = ""
//let workerQueue2 = DispatchQueue(label: "com.azaza.serega", qos: .userInitiated, attributes: .concurrent)
//let workerQueue3 = DispatchQueue(label: "com.azaza.serega", qos: .userInitiated, attributes: .concurrent)
//

//workerQueue2.async { task("tararararararara") }
//workerQueue3.async { task("----------------") }
//workerQueue3.async(execute: highPriorityItem)
//workerQueue2.async(execute: highPriorityItem2)

// Объявляем группу, в которой будут выполняться разные очереди, но результат будет получен, когда все потоки завершат свою работу. У нас сначала выводится принт, написанный после группы, а затем уже идет результат выполнения группы, которая была объявлена раньше.
//let firstGroup = DispatchGroup()
//DispatchQueue.global().async(group: firstGroup) {
//    print("azaza")
//    workerQueue2.async { task("tararararararara") }
//    workerQueue3.async { task("----------------") }
//    workerQueue3.async(execute: highPriorityItem)
//    workerQueue2.async(execute: highPriorityItem2)
//    print("The End!")
//}
//print("The End of stream")

//func addTwoInts(_ a: Int, _ b: Int) -> Int {
//    return a + b
//}
//
//var mathFunction: (Int, Int) -> Int = addTwoInts
//
//var function = addTwoInts
//addTwoInts(2, 3)
//mathFunction(2,3)
//function(2,3)
//
//func printMathResult(_ mathFunction: (Int, Int) -> Int, _ a: Int, _ b: Int) {
//    print("Result: \(mathFunction(a, b))")
//}
//printMathResult(addTwoInts, 2, 1)

//func printMathOp(_ a: Int, _ b: Int, _ c: String) -> (Int, Int) -> Int {
//    var result: Int
//    func addOp(_ one: Int, _ two: Int) -> Int { return one + two }
//    func multOp(_ one: Int, _ two: Int) -> Int { return one * two }
////    return c == "add" ? addOp : multOp
//    if c == "add" {
//        result = addOp(a,b)
//    } else {
//        result = multOp(a,b)
//    }
//    return result
//}

//printMathOp(2, 3, "mult")

//func addOp(_ one: Int, _ two: Int) -> Int { return one + two }
//addOp(1, 2)
//
// Тема: инициализаторы
//
//struct Size {
//    var w: Float = 0.0, h: Float = 0.0
//}
//
//struct Point {
//    var x: Float = 0.0, y: Float = 0.0
//}
//
//struct Rect {
//    var point = Point()
//    var size = Size()
//    init() {}
//    init(point: Point, size: Size) {
//        self.point = point
//        self.size = size
//    }
//    init(center: Point, size: Size) {
//        let pointX = center.x - (size.w / 2)
//        let pointY = center.y - (size.h / 2)
//        self.init(point: Point(x: pointX, y: pointY), size: size)
//    }
//}
//
//let firstRect = Rect()
//print(firstRect.point); print(firstRect.size); print("-----")
//let secondRect = Rect(point: Point(x: 2, y: 3), size: Size(w: 3, h: 2))
//print(secondRect.point); print(secondRect.size); print("-----")
//let thirdRect = Rect(center: Point(x: 2.2, y: 3.3), size: Size(w: 3.2, h: 2.2))
//print(thirdRect.point); print(thirdRect.size); print("-----")
//
//let c: Float = 2.2
//print(c)
//
////
//// Тема: Что будет проверять if?
////
//var isEmpty: Bool? = true
//if isEmpty! {
//    print(c)
//}
//
////
//// Тема: Что нужно возвращать Void?
////
//func newYear(x: String) -> Void? {
//    return nil
//}
//newYear(x: String(c))
//
////
//// Тема: Автозамыкания
////
//var cust = ["1","2","3","4","5"]
//print(cust.count)
//let custo = { cust.remove(at: 0) }
//print(cust.count)
//custo()
//print(cust.count)
//
//func serve(customer custo: () -> String) {
//    print(custo())
//}
//serve(customer: { cust.remove(at: 0) })
//
//func serve2(customer custo: @autoclosure () -> String) {
//    print(custo())
//}
//serve2(customer: cust.remove(at: 0))
//
//let age = -10
////assert(age >= 0, "error")
////precondition(age >= 0)
//
//var someInts = [Int]()
//someInts.append(1)
//someInts.append(2)
//someInts += [3]
//someInts = []
//someInts.isEmpty

//print("""
//---
//--- Это попытка синхронно добавить элементы во множество
//---
//""")
//var letters = Array<String>()
//letters.count
//let mySerialQueue = DispatchQueue(label: "com")
//let firstGroup = DispatchGroup()
//DispatchQueue.global().async(group: firstGroup) {
//    mySerialQueue.sync { letters.append("1") }
//    mySerialQueue.sync { letters.append("2") }
//    mySerialQueue.sync { letters.append("3") }
//    mySerialQueue.sync { letters.append("4") }
//    mySerialQueue.sync { letters.append("5") }
//    mySerialQueue.async { sleep(1) }
//}
//
//firstGroup.enter()
//firstGroup.notify(queue: mySerialQueue) {
//    mySerialQueue.async { letters.append("6") }
//    mySerialQueue.async { letters.append("7") }
//    mySerialQueue.async { letters.append("8") }
//    mySerialQueue.async { letters.append("9") }
//    mySerialQueue.async { letters.append("10") }
//}
//firstGroup.leave()
//letters.append("a")
//sleep(1)
//letters.append("b")
//for character in letters {
//    print(character)
//}

//enum CompassPoint {
//    case north
//    case south
//    case east
//    case west
//}
//var directionToHead = CompassPoint.east
//directionToHead = .north

// Неявно установленные исходные значения
//enum Planet: Int {
//    case Mercury = 1, Venus, Earth
//}
//let valueOfMercury = Planet.Mercury.rawValue
//Planet.Mercury
//let valueOfEarth = Planet.Earth.rawValue
//Planet.Earth
//
//// Структуры
//struct Resolution {
//    var width = 0
//    var height = 0
//}
//class VideoMode {
//    var resolution = Resolution()
//    var interlaced = false
//    var frameRate = 0.0
//    var name: String?
//}
//let myScreen = Resolution(width: 10, height: 20)
//print(myScreen)
//let tenEighty = VideoMode()
//tenEighty.resolution = myScreen
//tenEighty.interlaced = true
//tenEighty.name = "10x20p"
//tenEighty.frameRate = 25.0
//let newTenEighty = tenEighty
//newTenEighty.frameRate = 30.0
//print(tenEighty.frameRate)
//
//let sharedURLSession = URLSession.shared
//
//// Singletons
//class mySingleton {
//    static let shared = "Hello"
//}
//print(mySingleton.shared)
//
//
//class User {
//    var firstName = ""
//    var lastName = ""
//}
//
//class NetworkController {
//    let user: User
//    init(user: User) {
//        self.user = user
//    }
//}
//
//protocol FirstVCDelegate {
//    func passData(data: String)
//}
//
//class FirstVC {
//    var delegate: FirstVCDelegate?
//}
//
//class SecondVC: FirstVCDelegate {
//    func passData(data: String) {
//        print("The CEO gave me \(data)")
//    }
//}
//
//let firstVC = FirstVC()
//let secondVC = SecondVC()
//firstVC.delegate = secondVC
//firstVC.delegate?.passData(data: "a bunch of contracts")
//
//// Ten points to become better in SWIFT
//// 1
//extension Int {
//    var squared: Int { return self * self }
//}
//2.squared.squared.squared
//// 2
//func printElementFromArray<T>(_ a: [T]) {
//    for element in a { print(element) }
//}
////var intArray = [1,2,3,4,5,6,7,8,9,10]
////printElementFromArray(intArray)
//// 3
////for _ in 1...5 { print ("Count") }
//// 4 - if let VS. guard
//// 5 Computed Property VS. Function
//var radius: Double = 10
//var diameter: Double {
//    get { return radius * 2 } // если переменную вызвали
//    set { radius = newValue / 2 } // если переменной присвоили значение
//}
//radius
//diameter
//diameter = 1000
//radius
//diameter
//// 6
//// 7 Check for nil
//var userChosenColor: String?
//var defaultColor = "Red"
////userChosenColor = "Green"
//var colorToUse = userChosenColor ?? defaultColor
//// 8 Short If
//var defaultBool = false
//var height = 170 + (defaultBool ? 5 : 0)
//// 9 Functional Programming
//var evens = Array(1...10).filter { $0 % 2 == 0 }
//print(evens)
//// 10 Closure VS. Func
//func sum(x: Int, y: Int) -> Int { return x + y }
//var result = sum(x: 5, y: 6)
//var sumUsingClosure: (Int, Int) -> (Int) = { $0 + $1 }
//sumUsingClosure(5, 6)
//
//// Generic protocols with associated type
//struct GenericStruct<T> {
//    var property: T?
//}
//let a = GenericStruct<Bool>()
//let b = GenericStruct(property: "Bob")
//protocol NormalProtocol {
//    var property: String { get set }
//}
//class NormalClass: NormalProtocol {
//        var property: String = "Bob"
//}
//// Introducing Protocol Associated Types
//protocol GenericProtocol {
//    associatedtype myType
//    var anyProperty: myType { get set }
//}
//
//class SomeClass: GenericProtocol {
//    typealias myType = String
//    var anyProperty: myType = "Bob"
//}
//
//struct SomeStruct: GenericProtocol {
//    var anyProperty = 1996
//}
//
//extension GenericProtocol {
//    static func introduce() {
//        print("this print by extension")
//    }
//}
//
//SomeClass.introduce()
//SomeStruct.introduce()

////  НАЧАЛО РАЗБОРА RX SWIFT //
//// Imperative
//let myGrade = ["A", "B", "A"]
//var happyGrade: [String] = []
//for grade in myGrade {
//    if grade == "A" {
//        happyGrade.append(grade)
//    } else {
//        print("My mama ain't happy")
//    }
//}
//// Принт без переноса на новую строку
//print(happyGrade)
//// Declarative
//
//// Non-Reactive
//var a = 1
//var b = 3
//a + b
//a = 3
//
//// Наблюдатели свойства:
//class StepCounter {
//    var totalSteps: Int = 0 {
//        willSet(newTotalSteps) {
//            print("Значение будет равно: \(newTotalSteps)")
//        }
//        didSet {
//            if totalSteps > oldValue {
//                print("Было добавлено: \(totalSteps - oldValue)")
//            }
//        }
//    }
//}
//let stepCounter = StepCounter()
//stepCounter.totalSteps = 200
//stepCounter.totalSteps = 370
//
//class Counter {
//    var count = 0
//    func increment() {
//        count += 1
//    }
//    func increment(by amount: Int) {
//        count += amount
//    }
//    func reset() {
//        count = 0
//    }
//}
//
//struct Point {
//    var x = 0.0, y = 0.0
//    mutating func moveBy(x deltaX: Double, y deltaY: Double) {
//        self = Point(x: x + deltaX, y: y + deltaY)
//    }
//}
//
//enum TriStateSwitch {
//    case off, low, high
//    mutating func next() {
//        switch self {
//        case .off:
//            self = .low
//        case .low:
//            self = .high
//        case .high:
//            self = .off
//        }
//    }
//}
//var ovenLight = TriStateSwitch.low
//ovenLight.next()
//ovenLight.next()
//
///// Методы типа - ниже пример, который отслеживает прогресс игрока на разных уровнях игры
//struct LevelTracker {
//    static var highestUnlockedLevel = 1
//    /// Свойство экземпляра используется для отслеживания уровня на котором играет игрок
//    var currentLevel = 1
//
//    /// обновляет значение highestUnlockedLevel каждый раз, когда открывается новый уровень
//    static func unlock(_ level: Int) {
//        if level > highestUnlockedLevel { highestUnlockedLevel = level }
//    }
//
//    /// Возвращает true, если конкретный уровень уже разблокирован
//    static func isUnlocked(_ level: Int) -> Bool {
//        return level <= highestUnlockedLevel
//    }
//
//    /// До того как обновить currentLevel, этот метод проверяет доступен ли запрашиваемый новый уровень, метод возвращает логическое значение, указывающее, удалось ли ему поставить currentLevel. Не обязательно ошибка, если результат работы будет проигнорирован, потомушта метод имеет маркировку @discardableResult
//    @discardableResult
//    mutating func advance(to level: Int) -> Bool {
//        if LevelTracker.isUnlocked(level) {
//            currentLevel = level
//            return true
//        } else {
//            return false
//        }
//    }
//}
//
///// Используется для отслеживания и обновления прогресса конкретного игрока
//class Player {
//    var tracker = LevelTracker()
//    let playerName: String
//    func complete(level: Int) {
//        LevelTracker.unlock(level + 1)
//        tracker.advance(to: level + 1)
//    }
//    init(name: String) {
//        playerName = name
//    }
//}
//
//var player = Player(name: "Sergey Borisov")
//player.complete(level: 5)
//print("Игроку \(player.playerName) доступен уровень \(LevelTracker.highestUnlockedLevel)")
//
//player = Player(name: "Misha")
//if player.tracker.advance(to: 6) {
//    print("Игрок \(player.playerName) достигнул уровня 6")
//} else {
//    print("Уровень 6 заблокирован для \(player.playerName)")
//}
//
///// Использование сабскрипта
//struct TimeTable{
//    let multiplier: Int
//    subscript(index: Int, k: Int) -> Int {
//        return multiplier * index * k
//    }
//}
//
//let threeTimesTable = TimeTable(multiplier: 3)
//print(threeTimesTable[6,4])
//
//var numberOfLegs = ["паук": 8, "муравей": 6, "кошка": 4]
//numberOfLegs["птичка"] = 2
//let spiderIndex = numberOfLegs.index(forKey: "птичка")
//let spiderLegs = numberOfLegs["паук"]
//
///// Этот класс определяет базовые значения и функции для всех героев
//class Hero {
//    /// Текущие очки здоровья
//    var currentHP: Int = 0
//    /// Метод вовращающий значение очков здоровья
//    var description: String {
//        return "\(currentHP)"
//    }
//    /// Умение владеть мечом
//    var skillSword: Bool = false
//    /// Умение владеть посохом
//    var skillStaff: Bool = false
//
//    /// Функция нанесения урона (не переопределена для этого персонажа)
//    func makeDamage() {
//        // Ничего не делаем, так как не каждый герой умеет наносить урон
//    }
//    /// Функция лечения (не переопределена для этого персонажа)
//    func makeHeal() {
//        // Ничего не делаем, так как не каждый герой умеет лечить
//    }
//}
//
///// Класс WarriorHero наследуется от суперкласса Hero
//class WarriorHero: Hero {
//    /// Название класса персонажа
//    var nameClass = "Воин"
//    /// Базовое значение урона для этого персонажа
//    var baseDamage = 0.0
//
//    /// Специальный удар этого персонажа
//    override func makeDamage() {
//        print(nameClass, "наносит героический удар")
//    }
//}
//
///// Класс PriestHero наследуется от суперкласса Hero
//class PriestHero: Hero {
//    /// Название класса персонажа
//    var nameClass = "Жрец"
//    /// Базовое значение урона для этого персонажа
//    var baseDamage = 0.0
//
//    /// Специальное лечение этого персонажа
//    override func makeHeal() {
//        print(nameClass, "останавливает кровотечение бинтами")
//    }
//}
//
//let warriorHeroClass = WarriorHero()
//warriorHeroClass.currentHP = 100
//
//print("\(warriorHeroClass.nameClass) имеет \(warriorHeroClass.description) очков здоровья")
//
//if warriorHeroClass.skillSword == true {
//    print("Вы можете владеть мечом")
//} else {
//    print("Вы не можете владеть этим оружием")
//}
//
//final class FuryWarriorHero: WarriorHero {
//    /// Умение владеть двумя оружиями одновременно
//    var skillDoubleWeapon = true
//}
//
//let newCharacter = FuryWarriorHero()
//newCharacter.description
//newCharacter.makeDamage()
//let newHero = PriestHero()
//newHero.makeHeal()
//
////
//
//class Quest {
//    let text: String
//    var response: String?
//    init(text: String) {
//        self.text = text
//    }
//    /// Печать задания в консоль
//    func ask() {
//        print(text)
//    }
//}
//
//let newQuest = Quest(text: "Новое задание")
//newQuest.ask()
//
//class QuestReward: Quest {
//    let gold: Int
//    init(
//        /// Количества золота в награду за выполненное задание
//        gold: Int,
//        /// Описание задания
//        text: String) {
//
//        self.gold = gold
//        super.init(text: text)
//    }
//
//    func giveReward() {
//        print("Вы получили \(gold) золота за \(text)")
//    }
//}
//
//var newRewardQuest = QuestReward(gold: 10, text: "Ежедневное задание")
//newRewardQuest.giveReward()

//class Customer {
//    let name: String
//    var card: CreditCard?
//    init(name: String) {
//        self.name = name
//    }
//    deinit { print("\(name) деинициализируется") }
//}
//
//class CreditCard {
//    let number: UInt64
//    /// Бесхозная ссылка
//    unowned let customer: Customer
//    init(number: UInt64, customer: Customer) {
//        self.number = number
//        self.customer = customer
//    }
//    deinit { print("Карта #\(number) деинициализируется") }
//}
//
//var john: Customer?
//
//john = Customer(name: "John Appleseed")
//john!.card = CreditCard(number: 1234567890123456, customer: john!)
//
//john = nil

//class Person {
//    var residence: Residence?
//}
//
//class Room {
//    let name: String
//    init(name: String) { self.name = name }
//}
//
//class Address {
//    var buildingName: String?
//    var buildingNumber: String?
//    var street: String?
//    func buildingIdentifier() -> String? {
//        if let buildingNumber = buildingName, let street = street {
//            return "\(buildingNumber) \(street)"
//        } else if buildingName != nil {
//            return buildingName
//        } else {
//            return nil
//        }
//    }
//}
//
//class Residence {
//    var rooms = [Room]()
//    var numberOfRooms: Int {
//        return rooms.count
//    }
//    subscript(i: Int) -> Room {
//        get {
//            return rooms[i]
//        }
//        set {
//            rooms[i] = newValue
//        }
//    }
//    func printNumberOfRooms() {
//        print("numberOfRooms")
//    }
//    var address: Address?
//}
//
//let john = Person()
//
//if let roomCount = john.residence?.numberOfRooms {
//    print(roomCount)
//} else {
//    print("error")
//}
//
//john.residence = Residence()
//
//if let roomCount = john.residence?.numberOfRooms {
//    print(roomCount)
//} else {
//    print("II error")
//}
//
//func printA() {
//    print("A?")
//}
//
//let defaults = UserDefaults.standard
//defaults.set(25, forKey: "Age")
//defaults.set(true, forKey: "UseTouchID")
//defaults.set(CGFloat.pi, forKey: "Pi")
//
//let age = defaults.integer(forKey: "Age")
//let useTouchID = defaults.bool(forKey: "UseTouchID")
//let pi = defaults.double(forKey: "Pi")

/// Замыкания
//let closure = {
//    print("Hello, World!")
//}
//
//func repeatThreeTimes(closure: () -> ()) {
//    for _ in 0...2 {
//        closure()
//    }
//}
//
//repeatThreeTimes {
//    () -> () in
//    print("Hello, World!")
//}
//
//let unsortedArray = [123, 2, 32, 67, 8797, 432]
//let sortedArray = unsortedArray.sorted {
//    (number1: Int, number2: Int) -> Bool in
//    return number1 > number2
//}

/// 35. Обработка ошибок и отложенные действия (15:22)
//enum PossibleErrors: Error {
//    case NotInStock
//    case NotEnoughMoney
//}
//
//struct Book {
//    let price: Int
//    var count: Int
//}
//
//class Library {
//    var deposit = 11
//    var libraryBooks = ["Book1": Book(price: 10, count: 1), "Book2": Book(price: 11, count: 0), "Book3": Book(price: 12, count: 3)]
//
//    func getTheBook(withName: String) throws {
//        guard var book = libraryBooks[withName] else {
//            throw PossibleErrors.NotInStock
//        }
//
//        guard book.count > 0 else {
//            throw PossibleErrors.NotInStock
//        }
//
//        guard book.price <= deposit else {
//            throw PossibleErrors.NotEnoughMoney
//        }
//
//        deposit -= book.price
//        book.count -= 1
//        libraryBooks[withName] = book
//        print("You got the Book: \(withName)")
//    }
//}
//
//let library = Library()
//try? library.getTheBook(withName: "Book1")
//library.deposit
//library.libraryBooks
//
//do {
//    try library.getTheBook(withName: "Book3")
//} catch PossibleErrors.NotInStock {
//    print("Book is not in stock")
//} catch PossibleErrors.NotEnoughMoney {
//    print("Not enough money")
//}
//
//func doConnection() throws -> Int {
//    return 10
//}
//
//let x = try? doConnection()
//
//var y: Int?
//
//do {
//    y = try doConnection()
//} catch {
//    y = nil
//}
//
//var attempt = 0
//func whateverFunc(param: Int) -> Int {
//    defer {
//        attempt += 2
//        print("Прибавили два")
//    }
//
//    defer {
//        attempt *= 10
//        print("Умножили на десять")
//    }
//
//    defer {
////        attempt -= 5
////        print("Отняли пять")
//    }
//
//    switch param {
//    case 0:  return attempt
//    case 1:  return attempt
//    default: return attempt
//    }
//}
//
//whateverFunc(param: 2)
//attempt
//whateverFunc(param: 1)
//attempt
//whateverFunc(param: 2)
//attempt

/// 36. Сабскрипты (6:16)
//struct WorkPlace {
//
//    var table: String
//    var workPlace: [String]
//
//    subscript(index: Int) -> String? {
//        get {
//            switch index {
//            case 0: return table
//            case 1..<workPlace.count + 1: return workPlace[index - 1]
//            default: return nil
//            }
//        }
//
//        set {
//            switch index {
//            case 0 : return table = newValue ?? ""
//            case 1..<workPlace.count + 1: return workPlace[index - 1] = newValue ?? ""
//            default: break
//            }
//        }
//    }
//
//}
//
//var work = WorkPlace(table: "table", workPlace: ["chair", "armchair", "lamp"])
//work.workPlace[1]
//work[1]
//
//work.table
//work[0]
//
//work.workPlace
//work.workPlace[1] = "TV"
//work.workPlace
//work[1] = "PS4"
//work.workPlace
//for a in 0...3 {
//    print(work[a])
//}
