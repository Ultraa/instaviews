
мне нужна идея что делать дальше с accessToken'ом:

Вопрос:
загружать данные в хранимую структуру и уже из структуры заполнять ячейки?
Ответ: 
получать json, парсить его в структуру, из структуры уже заполняем ячейки



По пунктам:

// во ViewController'e, там куда мы передали accessToken, начинаем писать url запрос
// ВНИМАНИЕ! ЕСЛИ У НАС HTTP-ЗАПРОС, ТО БУДЕТ ОШИБКА! НУЖНО ДОБАВИТЬ РАЗРЕШЕНИЕ В INFO.PLIST, ЕСЛИ ЗАПРОС HTTPS, ТО ВСЁ ОК!
// пишем в основном классе во ViewDidLoad():

let urlString = "......" // тут будет адрес с использованием полученного accessToken
guard let url = URL(string: urlString) else { return } // получаем URL

URLSession.shared.dataTask(with: url) { (data, response, error) in

    guard let data = data else { return } // если ничего не получим, то выйдем
    guard error == nil else { return } // защита от возможных падений от ошибок
    
    do {
    let information = try JSONDecoder().decode(InstaData.self, from: data) // в .decode()  пишем свой тип данных (который описывает структуру JSON-файла), а в from: пишем data - то есть куда мы получили данные из URL-запроса
        print(information)
    } catch let error {
        print(error)
    }
}.resume()


// пишем выше объявления класса:

struct InstaData: Decodable {
    var photo: String?
    var description: String?
    var location: CLLocation2...
}

// а можем также сделать вложенные структуры:

struct InstaData: Decodable {
    var photo: SomeData?
    var description: SomeData?
    var location: SomeData?
}

struct SomeData: Decodable {
    var text: String?
}
